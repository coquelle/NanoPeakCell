# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Stats.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(640, 767)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(Form.sizePolicy().hasHeightForWidth())
        Form.setSizePolicy(sizePolicy)
        Form.setSizeIncrement(QtCore.QSize(1, 1))
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.tabWidget = QtWidgets.QTabWidget(Form)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.tab)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.HitRateView = PlotWidget(self.tab)
        self.HitRateView.setEnabled(True)
        self.HitRateView.setMinimumSize(QtCore.QSize(600, 600))
        self.HitRateView.setSizeIncrement(QtCore.QSize(1, 1))
        self.HitRateView.setObjectName("HitRateView")
        self.gridLayout_2.addWidget(self.HitRateView, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.Linear_View = GraphicsView(self.tab_2)
        self.Linear_View.setEnabled(True)
        self.Linear_View.setGeometry(QtCore.QRect(10, 10, 600, 602))
        self.Linear_View.setMinimumSize(QtCore.QSize(600, 600))
        self.Linear_View.setSizeIncrement(QtCore.QSize(1, 1))
        self.Linear_View.setObjectName("Linear_View")
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_3)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.Heat_Map = ImageView(self.tab_3)
        self.Heat_Map.setEnabled(True)
        self.Heat_Map.setMinimumSize(QtCore.QSize(600, 600))
        self.Heat_Map.setSizeIncrement(QtCore.QSize(1, 1))
        self.Heat_Map.setObjectName("Heat_Map")
        self.gridLayout_3.addWidget(self.Heat_Map, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_3, "")
        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 1)
        self.tableWidget = QtWidgets.QTableWidget(Form)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        self.tableWidget.verticalHeader().setVisible(False)
        self.gridLayout.addWidget(self.tableWidget, 1, 0, 1, 1)

        self.retranslateUi(Form)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("Form", "Hit Rate vs time"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("Form", "Linear View"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("Form", "Heat map"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("Form", "Run #"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("Form", "# processed"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("Form", "# hits"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("Form", "Progress (%)"))
from pyqtgraph import PlotWidget, GraphicsView, ImageView

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow_NoMenu.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(860, 720)
        MainWindow.setSizeIncrement(QtCore.QSize(0, 0))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.graphicsView = GraphicsView(self.centralwidget)
        self.graphicsView.setMinimumSize(QtCore.QSize(600, 600))
        self.graphicsView.setSizeIncrement(QtCore.QSize(1, 1))
        self.graphicsView.setObjectName("graphicsView")
        self.verticalLayout_2.addWidget(self.graphicsView)
        self.ImageInfo = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Courier New")
        self.ImageInfo.setFont(font)
        self.ImageInfo.setObjectName("ImageInfo")
        self.verticalLayout_2.addWidget(self.ImageInfo)
        self.Mask = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Courier New")
        self.Mask.setFont(font)
        self.Mask.setObjectName("Mask")
        self.verticalLayout_2.addWidget(self.Mask)
        self.Geom = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Courier New")
        self.Geom.setFont(font)
        self.Geom.setObjectName("Geom")
        self.verticalLayout_2.addWidget(self.Geom)
        self.horizontalLayout_3.addLayout(self.verticalLayout_2)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setSpacing(20)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setMinimumSize(QtCore.QSize(220, 200))
        self.groupBox.setMaximumSize(QtCore.QSize(220, 200))
        self.groupBox.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.groupBox.setObjectName("groupBox")
        self.layoutWidget = QtWidgets.QWidget(self.groupBox)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 30, 209, 151))
        self.layoutWidget.setObjectName("layoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.Min = QtWidgets.QLineEdit(self.layoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Min.sizePolicy().hasHeightForWidth())
        self.Min.setSizePolicy(sizePolicy)
        self.Min.setText("0")
        self.Min.setObjectName("Min")
        self.gridLayout.addWidget(self.Min, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.layoutWidget)
        self.label_3.setMaximumSize(QtCore.QSize(100, 16777215))
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)
        self.Max = QtWidgets.QLineEdit(self.layoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Max.sizePolicy().hasHeightForWidth())
        self.Max.setSizePolicy(sizePolicy)
        self.Max.setText("10")
        self.Max.setObjectName("Max")
        self.gridLayout.addWidget(self.Max, 1, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.label_6 = QtWidgets.QLabel(self.layoutWidget)
        self.label_6.setMaximumSize(QtCore.QSize(100, 20))
        self.label_6.setObjectName("label_6")
        self.verticalLayout.addWidget(self.label_6)
        self.ColorMap = QtWidgets.QComboBox(self.layoutWidget)
        self.ColorMap.setMaximumSize(QtCore.QSize(170, 16777215))
        self.ColorMap.setObjectName("ColorMap")
        self.ColorMap.addItem("")
        self.ColorMap.addItem("")
        self.ColorMap.addItem("")
        self.ColorMap.addItem("")
        self.ColorMap.addItem("")
        self.ColorMap.addItem("")
        self.ColorMap.addItem("")
        self.ColorMap.addItem("")
        self.ColorMap.addItem("")
        self.ColorMap.addItem("")
        self.verticalLayout.addWidget(self.ColorMap)
        self.Reset = QtWidgets.QPushButton(self.layoutWidget)
        self.Reset.setObjectName("Reset")
        self.verticalLayout.addWidget(self.Reset)
        self.verticalLayout_4.addWidget(self.groupBox, 0, QtCore.Qt.AlignTop)
        self.Stream = QtWidgets.QGroupBox(self.centralwidget)
        self.Stream.setMinimumSize(QtCore.QSize(220, 220))
        self.Stream.setMaximumSize(QtCore.QSize(220, 200))
        self.Stream.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.Stream.setObjectName("Stream")
        self.layoutWidget_2 = QtWidgets.QWidget(self.Stream)
        self.layoutWidget_2.setGeometry(QtCore.QRect(10, 30, 269, 174))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.layoutWidget_2)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label = QtWidgets.QLabel(self.layoutWidget_2)
        self.label.setObjectName("label")
        self.verticalLayout_3.addWidget(self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.radioButton1 = QtWidgets.QRadioButton(self.layoutWidget_2)
        self.radioButton1.setMinimumSize(QtCore.QSize(90, 0))
        self.radioButton1.setMaximumSize(QtCore.QSize(90, 16777215))
        self.radioButton1.setChecked(True)
        self.radioButton1.setObjectName("radioButton1")
        self.horizontalLayout.addWidget(self.radioButton1)
        self.radioButton2 = QtWidgets.QRadioButton(self.layoutWidget_2)
        self.radioButton2.setObjectName("radioButton2")
        self.horizontalLayout.addWidget(self.radioButton2)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.label_4 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_3.addWidget(self.label_4)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(25)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.DetectedPeaks = QtWidgets.QCheckBox(self.layoutWidget_2)
        self.DetectedPeaks.setMinimumSize(QtCore.QSize(90, 0))
        self.DetectedPeaks.setMaximumSize(QtCore.QSize(90, 16777215))
        self.DetectedPeaks.setObjectName("DetectedPeaks")
        self.horizontalLayout_2.addWidget(self.DetectedPeaks)
        self.InetgratedPeaks = QtWidgets.QCheckBox(self.layoutWidget_2)
        self.InetgratedPeaks.setObjectName("InetgratedPeaks")
        self.horizontalLayout_2.addWidget(self.InetgratedPeaks)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.Nfiles = QtWidgets.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.Nfiles.setFont(font)
        self.Nfiles.setObjectName("Nfiles")
        self.verticalLayout_3.addWidget(self.Nfiles)
        self.NPeaks = QtWidgets.QLabel(self.layoutWidget_2)
        self.NPeaks.setText("")
        self.NPeaks.setObjectName("NPeaks")
        self.verticalLayout_3.addWidget(self.NPeaks)
        self.NInt = QtWidgets.QLabel(self.layoutWidget_2)
        self.NInt.setText("")
        self.NInt.setObjectName("NInt")
        self.verticalLayout_3.addWidget(self.NInt)
        self.verticalLayout_4.addWidget(self.Stream)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem)
        self.horizontalLayout_3.addLayout(self.verticalLayout_4)
        self.horizontalLayout_4.addLayout(self.horizontalLayout_3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionLoad_Images = QtWidgets.QAction(MainWindow)
        self.actionLoad_Images.setObjectName("actionLoad_Images")
        self.actionLoad_Geometry = QtWidgets.QAction(MainWindow)
        self.actionLoad_Geometry.setObjectName("actionLoad_Geometry")
        self.actionClose = QtWidgets.QAction(MainWindow)
        self.actionClose.setObjectName("actionClose")
        self.actionBeam_Center = QtWidgets.QAction(MainWindow)
        self.actionBeam_Center.setCheckable(True)
        self.actionBeam_Center.setChecked(False)
        self.actionBeam_Center.setIconVisibleInMenu(False)
        self.actionBeam_Center.setObjectName("actionBeam_Center")
        self.actionResolution_Rings = QtWidgets.QAction(MainWindow)
        self.actionResolution_Rings.setCheckable(True)
        self.actionResolution_Rings.setChecked(True)
        self.actionResolution_Rings.setObjectName("actionResolution_Rings")
        self.actionBragg_Peaks = QtWidgets.QAction(MainWindow)
        self.actionBragg_Peaks.setObjectName("actionBragg_Peaks")
        self.actionFile_Tree = QtWidgets.QAction(MainWindow)
        self.actionFile_Tree.setCheckable(True)
        self.actionFile_Tree.setChecked(True)
        self.actionFile_Tree.setObjectName("actionFile_Tree")
        self.actionExperimental_Settings = QtWidgets.QAction(MainWindow)
        self.actionExperimental_Settings.setCheckable(True)
        self.actionExperimental_Settings.setChecked(True)
        self.actionExperimental_Settings.setObjectName("actionExperimental_Settings")
        self.actionHit_Finding = QtWidgets.QAction(MainWindow)
        self.actionHit_Finding.setCheckable(True)
        self.actionHit_Finding.setObjectName("actionHit_Finding")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "NanoPeakCell"))
        self.ImageInfo.setText(_translate("MainWindow", "x:    y:    - Res:    - I: "))
        self.Mask.setText(_translate("MainWindow", "Current Mask File:  N/A"))
        self.Geom.setText(_translate("MainWindow", "Current Geometry File: N/A"))
        self.groupBox.setTitle(_translate("MainWindow", "Viewer Settings"))
        self.label_2.setText(_translate("MainWindow", "Min Value"))
        self.label_3.setText(_translate("MainWindow", "Max Value"))
        self.label_6.setText(_translate("MainWindow", "Color Map"))
        self.ColorMap.setItemText(0, _translate("MainWindow", "Hot"))
        self.ColorMap.setItemText(1, _translate("MainWindow", "Gray"))
        self.ColorMap.setItemText(2, _translate("MainWindow", "Gray_r"))
        self.ColorMap.setItemText(3, _translate("MainWindow", "Blues"))
        self.ColorMap.setItemText(4, _translate("MainWindow", "Blues_r"))
        self.ColorMap.setItemText(5, _translate("MainWindow", "Reds"))
        self.ColorMap.setItemText(6, _translate("MainWindow", "Reds_r"))
        self.ColorMap.setItemText(7, _translate("MainWindow", "Jet"))
        self.ColorMap.setItemText(8, _translate("MainWindow", "Spectral"))
        self.ColorMap.setItemText(9, _translate("MainWindow", "Spectral_r"))
        self.Reset.setText(_translate("MainWindow", "Reset Zoom"))
        self.Stream.setTitle(_translate("MainWindow", "Stream"))
        self.label.setText(_translate("MainWindow", "Display:"))
        self.radioButton1.setText(_translate("MainWindow", "All Images"))
        self.radioButton2.setText(_translate("MainWindow", " Only Indexed "))
        self.label_4.setText(_translate("MainWindow", "Display Bragg peaks:"))
        self.DetectedPeaks.setText(_translate("MainWindow", "Spots"))
        self.InetgratedPeaks.setText(_translate("MainWindow", "Reflections"))
        self.Nfiles.setText(_translate("MainWindow", "Stream info: "))
        self.actionLoad_Images.setText(_translate("MainWindow", "Load Images"))
        self.actionLoad_Geometry.setText(_translate("MainWindow", "Load Geometry"))
        self.actionClose.setText(_translate("MainWindow", "Close"))
        self.actionBeam_Center.setText(_translate("MainWindow", "Beam Center"))
        self.actionResolution_Rings.setText(_translate("MainWindow", "Resolution Rings"))
        self.actionBragg_Peaks.setText(_translate("MainWindow", "Bragg Peaks"))
        self.actionFile_Tree.setText(_translate("MainWindow", "File Tree"))
        self.actionExperimental_Settings.setText(_translate("MainWindow", "Experimental Settings"))
        self.actionHit_Finding.setText(_translate("MainWindow", "Hit Finding"))
from pyqtgraph import GraphicsView

import multiprocessing
import time
from xmlrpc.client import ServerProxy
from pyqtgraph import ptime
import zmq
import numpy as np
import h5py
import requests
import json
from NPC.utils import Log
from NPC.gui.ZmqSockets import ZmqTimerManager, ZmqTimer, EigerStreamDecoder
import fabio
import msgpack
import msgpack_numpy as m
import os
import threading
import queue


def openMask(fn):
    h51 = h5py.File(fn, 'r')
    data = -1 * (h51['data'][:] - 1)
    h51.close()
    return data.astype(np.int32)


# noinspection PyAttributeOutsideInit
class NPC_Online_Worker(multiprocessing.Process):

    def __init__(self, wrk_num, setup):
        super().__init__()
        self.wrk_num = wrk_num
        self.setup = setup
        self.ip_address_viewer = setup['viewer_ip']
        self.shape = setup['shape']
        self.MP = np.zeros(self.shape)

        # default PF8 parameters
        self.params = {"cycles": 3,
                       "cutoff_pick": 5,
                       "noise": 15,
                       "connected": 1,
                       "patch_size": 3,
                       "min_N_spots": 10}

    # noinspection PyAttributeOutsideInit
    def setup_zmq_sockets(self):
        self.context0 = zmq.Context()

        # The connection of the streamReceiver will be done in a subclass
        self.streamReceiver = self.context0.socket(zmq.PULL)

        # Set up a channel to receive control messages over directly from the GUI (SUB socket @ port 5559)
        self.control_receiver = self.context0.socket(zmq.SUB)
        self.control_receiver.connect("tcp://%s:5559" % self.ip_address_viewer)
        self.control_receiver.subscribe("") # Subscribe to all messages

        # Set up a channel to send messages to the load balancer (including images)
        self.backend = zmq.Context().socket(zmq.DEALER)
        self.backend.identity = u"Worker-{}".format(self.wrk_num).encode("ascii")
        self.backend.connect("tcp://%s:5571" % self.ip_address_viewer)

        self.MPbackend = zmq.Context().socket(zmq.DEALER)
        self.MPbackend.identity = u"Worker-{}".format(self.wrk_num).encode("ascii")
        self.MPbackend.connect("tcp://%s:5561" % self.ip_address_viewer)

        # Set up a channel to send result of workers to the Stats Load Balancer (DEALER socket @ port 5551)
        self.stats_backend = zmq.Context().socket(zmq.DEALER)
        self.stats_backend.identity = u"Worker-{}".format(self.wrk_num).encode("ascii")
        self.stats_backend.connect("tcp://%s:5551" % self.ip_address_viewer)

        self.crystfel = zmq.Context().socket(zmq.DEALER)
        self.crystfel.identity = u"Worker-{}".format(self.wrk_num).encode("ascii")
        #self.crystfel.bind("tcp://*:5501")

    # This will be overwritten in the subclass
    def connect_receiver(self):
        self.streamReceiver.connect("tcp://%s:9999" % self.ip_address_viewer)

    def decode_stream(self):
        message = self.streamReceiver.recv_json()
        self.frameID = message['file_number'] - 1
        self.fn = message['filename']
        #raw = self.cbf.read(self.fn, only_raw=True)
        #return self.bo(raw, as_float=True).reshape(self.setup["shape"])
        return fabio.open(self.fn).data

    # noinspection PyPep8Naming
    def run(self, *args, **kwargs):
        from NPC.SpotFinders import dozor, PeakFinder8
        self.setup_zmq_sockets()
        self.connect_receiver()
        self.send_start_to_backends()

        if self.setup["SpotFinder"] == 'Dozor':
            self.spotfinder = dozor.Dozor()
        else:
            import pyopencl as cl
            #from silx.opencl.codec.byte_offset import ByteOffset
            os.environ["PYOPENCL_COMPILER_OUTPUT"] = "1"
            os.environ["PYOPENCL_CTX"] = '0:1'
            ctx = cl.create_some_context(interactive=False)
            self.spotfinder = PeakFinder8.PF8(det=self.setup["detector"], ctx=ctx)
            #size = self.setup["shape"][0] * self.setup["shape"][1]
            #self.bo = ByteOffset(2478482, size, ctx=ctx)
            #self.cbf = fabio.cbfimage.CbfImage()


        Nprocessed = 0
        Nhits = 0
        spotfinder_READY = False
        p0 = ptime.time()
        self.hitsOnly = True
        self.Nrequest = 0
        data2SND = True
        spotsMP = []
        spots2SND = []
        data = None
        hit = None
        results = {'hits': [],
                   'nPeaks': [],
                   'spots': [],
                   'file_number': [],
                   'filenames': []
                   }
        filenames = []
        file_number = []
        hits = []
        nPeaks = []
        spots_results = []

        poller = zmq.Poller()
        poller.register(self.streamReceiver, zmq.POLLIN)
        poller.register(self.control_receiver, zmq.POLLIN)
        poller.register(self.backend, zmq.POLLIN)
        poller.register(self.MPbackend, zmq.POLLIN)
        poller.register(self.stats_backend, zmq.POLLIN)
        poller.register(self.crystfel, zmq.POLLIN)


        while True:
            socks = dict(poller.poll(0))


            if socks.get(self.crystfel) == zmq.POLLIN:
                try:
                    _ = self.crystfel.recv(zmq.NOBLOCK)
                    self.Nrequest += 1
                    Log("Worker #%i received request (#%s): " % (self.wrk_num, str(self.Nrequest).zfill(4)))
                    #Here send crystfel request
                    self.crystfel.send(b'next')

                except:
                    Log("No request from crystfel")
                    pass

            # If the message comes from ventilator
            if socks.get(self.streamReceiver) == zmq.POLLIN:
                data = self.decode_stream()
                "Zmq stream from Eiger sends message at start and end of data collection"
                if isinstance(data, str):
                    continue

                if spotfinder_READY:
                    if self.setup["SpotFinder"] == 'Dozor':
                        spots = self.spotfinder.find_spots(data.astype(np.uint16))
                    else:
                        spots = self.spotfinder.find_spots(data, params=self.params)

                    Nprocessed += 1
                    _nPeaks = len(spots["pos0"])
                    IsHit = _nPeaks >= self.params["min_N_spots"]
                    filenames.append(self.fn)
                    file_number.append(self.frameID)
                    hits.append(IsHit)
                    nPeaks.append(_nPeaks)
                    spots_results.append(spots)
                    if self.wrk_num == 0:
                        Log("Worker %i processing filename %s. Option %s" %(self.wrk_num, self.fn, self.hitsOnly))


                    if IsHit:
                        Nhits += 1
                        #if self.hitsOnly:
                        spotsMP.append(spots)
                        self.MP[:] = np.maximum(data, self.MP)
                        hit = data.copy()
                        spots2SND = spots
                        if data2SND:
                            self.backend.send(b"READY")
                            self.crystfel.send(b"READY")
                            data2SND = False
                    else:
                        if not self.hitsOnly:
                            hit = data.copy()
                            spots2SND = ([], [], [])
                            if data2SND:
                                self.backend.send(b"READY")
                                self.crystfel.send(b"READY")
                                data2SND = False

            # If the message comes from the UI
            if socks.get(self.control_receiver) == zmq.POLLIN:
                newParams = self.control_receiver.recv_json(zmq.NOBLOCK)
                key = list(newParams.keys())[0]

                # The GUI should directly communicate to the MP Load Balancer
                if key == "resetMP":
                    if self.wrk_num == 0:
                        self.MPbackend.send(b"resetMP")
                        Log("Message received from Control Gui: Resetting MP")
                    self.MP[:] = 0

                elif key == "STOP":
                    if self.wrk_num == 0:
                        Log("Message received from Control Gui: Exiting")
                    break

                elif key == "hitsOnly":
                        self.hitsOnly = newParams[key]
                        Log("Worker %i changed to %s" % (self.wrk_num, str(self.hitsOnly)))
                else:
                    for key in newParams.keys():
                        self.params[key] = newParams[key]
                        if self.wrk_num == 0:
                            Log("Message received from Control Gui: Setting %s to value %s" % (key,str(newParams[key])))

            if socks.get(self.stats_backend) == zmq.POLLIN:
                mxcube = self.stats_backend.recv_json()
                Log("Worker %i received mxcube message" % self.wrk_num)
                self.spotfinder.setup_geom(mxcube)
                spotfinder_READY = True
                Log("SpotFinder ready")


            # If the message comes from the load balancer (backend)
            if socks.get(self.backend) == zmq.POLLIN:

                _, address, _, request = self.backend.recv_multipart()






                if hasattr(data, 'shape'):
                    map_object = {'data': hit,
                                  'spots': spots2SND}
                    packed = msgpack.packb(map_object, default=m.encode)
                    self.backend.send(address, flags=zmq.SNDMORE)
                    self.backend.send(packed)
                    data2SND = True

            # If the message comes from the MP load balancer
            if socks.get(self.MPbackend) == zmq.POLLIN:
                _, address, _, request = self.MPbackend.recv_multipart()
                MP_map_object = {'MP': self.MP}
                packed = msgpack.packb(MP_map_object, default=m.encode)
                self.MPbackend.send(address, flags=zmq.SNDMORE)
                self.MPbackend.send(packed)

            # If enough time elapsed... send stats via the result channel @ 5558 (PUSH socket)
            p1 = ptime.time()
            if p1 - p0 > 1.88:
                if Nprocessed > 0:
                    results['filenames'] = filenames
                    results['hits'] = hits
                    results['nPeaks'] = nPeaks
                    results['spots'] = spots_results
                    results['file_number'] = file_number
                    filenames = []
                    hits = []
                    nPeaks = []
                    spots_results = []
                    file_number = []
                    self.stats_backend.send(msgpack.packb(results, default=m.encode))
                    Nprocessed = 0
                    Nhits = 0
                    p0 = p1

        self.backend.close()
        self.MPbackend.close()
        self.streamReceiver.close()
        self.stats_backend.close()
        self.control_receiver.close()
        self.context0.term()
        Log("Worker %i exited properly" % self.wrk_num)
        return

    def send_start_to_backends(self):
        self.MPbackend.send(b"START")
        self.stats_backend.send(b"START")
        # send a start message to receive the parameters from the gui at startup...
        self.backend.send(b"START")
        newParams = self.backend.recv_json()
        for key, value in newParams.items():
            if key == "hitsOnly":
                self.hitsOnly = value
                Log("Worker #%i changed to %s" % (self.wrk_num, str(self.hitsOnly)))
            else:
                self.params[key] = value



class NPC_Online_Eiger_Worker(NPC_Online_Worker):

    def __init__(self, wrk_num, setup):
        super().__init__(wrk_num, setup)
        self.wrk_num = wrk_num
        self.setup = setup
        self.eiger_ip = setup['eiger_ip']
        #self.streamDecoder = EigerStreamDecoder()
        self.h5fn = None

    # This will be overwritten in the subclass
    def connect_receiver(self):
        self.streamReceiver.connect("tcp://%s:9999" % self.ip_address_viewer)

    def decode_stream(self):
        message = self.streamReceiver.recv_json()
        self.frameID = message['file_number']
        self.idx = message['index']
        self.fn = message['filename']
        if self.h5fn != self.fn:
           self.h5fn = self.fn
           self.h5 = h5py.File(self.fn, 'r')
        return self.h5['entry/data/data'][self.idx,::]


# This class is intended for receiver zmq stream directly from Eiger
# class NPC_Online_Eiger_Worker(NPC_Online_Worker):
#
#    def __init__(self, wrk_num, setup):
#        super().__init__(wrk_num, setup)
#        self.wrk_num = wrk_num
#        self.setup = setup
#        self.eiger_ip = setup['eiger_ip']
#        self.streamDecoder = EigerStreamDecoder()
#
#    # This will be overwritten in the subclass
#    def connect_receiver(self):
#        self.streamReceiver.connect("tcp://%s:9999" % self.eiger_ip)
#
#    def decode_stream(self):
#        frames = self.streamReceiver.recv_multipart(copy=False)
#        seriesID, frameID, data = self.streamDecoder.decodeFrames(frames)
#        self.frameID = frameID * seriesID
#        if not isinstance(data, str):
#            data[data > 65000] = 0
#        self.fn = 'tmp_Eiger_%i' %self.frameID
#        return data


def loadBalancer(NCpus,startup):

    """Load balancer main loop."""

    # Prepare context and sockets
    Log("Load Balancer Started")
    # frontend is connected to the GUI @ 5570
    context = zmq.Context.instance()
    frontend = context.socket(zmq.ROUTER)
    frontend.bind('tcp://*:5570')

    # backend is connected to the workers @ 5571
    backend = context.socket(zmq.ROUTER)
    backend.bind("tcp://*:5571")


    workers = []
    i = 0
    gui_data = None
    poller = zmq.Poller()
    poller.register(backend, zmq.POLLIN)
    poller.register(frontend, zmq.POLLIN)
    while True:
        sockets = dict(poller.poll())

        # Handle worker activity on the backend
        # In this zmq socket, the first element of the message is its origin (identity of the sender, here Worker-i
        if backend in sockets:
            request = backend.recv_multipart()
            worker, message = request[:2]

            if message == b"START":  # At startup, sending the parameters back
                backend.send(worker, flags=zmq.SNDMORE)
                backend.send_json(startup)

            # Once a worker finds a hit, it sends a b"READY" message to the load balancer
            # The load balancer adds it to the 'available' workers
            # and register the frontend for polling now that a worker was available
            elif message == b"READY":
                if worker not in workers:
                    workers.append(worker)

            else:
                # This is a message corresponding to a hit
                packed = request[2]
                frontend.send_multipart([client, b"", packed])

        if frontend in sockets:

            # Get next client request
            client, _, request = frontend.recv_multipart()
            if request == b"HIT":
                # route to last-used worker
                if workers:
                    worker = workers.pop(0)
                    backend.send_multipart([worker, b"", client, b"", request])
                    i += 1
                else:
                    frontend.send_multipart([client, b"", request])

            if request == b"STOP":
                frontend.send_multipart([client, b"", request])
                Log("Load Balancer: Stop message received")
                break


    # Clean up at exit
    backend.close()
    frontend.close()
    context.term()
    Log("Load Balancer exited properly")


def crystfelLoadBalancer(NCpus,startup):

    """Load balancer main loop."""

    # Prepare context and sockets
    Log("CrystFEL Load Balancer Started")
    # crystfel socket is connected to the GUI @ 5500
    context = zmq.Context.instance()
    crystfel = context.socket(zmq.ROUTER)
    crystfel.bind('tcp://*:5500')

    # backend is connected to the workers @ 5501
    backend = context.socket(zmq.ROUTER)
    backend.bind("tcp://*:5501")


    workers = []
    i = 0
    poller = zmq.Poller()
    poller.register(backend, zmq.POLLIN)
    poller.register(crystfel, zmq.POLLIN)
    while True:
        sockets = dict(poller.poll())

        # Handle worker activity on the backend
        # In this zmq socket, the first element of the message is its origin (identity of the sender, here Worker-i
        if backend in sockets:
            request = backend.recv_multipart()
            worker, message = request[:2]

            # Once a worker finds a hit, it sends a b"READY" message to the load balancer
            # The load balancer adds it to the 'available' workers
            # and register the frontend for polling now that a worker was available
            if message == b"READY":
                if worker not in workers:
                    workers.append(worker)

            else:
                # This is a message corresponding to a hit
                packed = request[2]
                crystfel.send_multipart([client, b"", packed])
                Log("CrystFEL balancer sent image")

        if crystfel in sockets:
            client, _, request = crystfel.recv_multipart()
            if request == b"next":
                #Log("CrystFEL balancer received a crystfel request")

            # route to last-used worker
                if workers:
                    worker = workers.pop(0)
                    backend.send_multipart([worker, b"", client, b"", request])
                    Log("CrystFEL balancer sent a request to worker %s" %str(worker))
                    i += 1
                else:
                    crystfel.send_multipart([client, b"", request])


    # Clean up at exit
    backend.close()
    crystfel.close()
    context.term()
    Log("Load Balancer exited properly")



def send_multi(socket, origin, destination, request):
    getattr(socket,"send_multipart")([origin, b"", destination, b"", request])
    #socket.send_multipart([origin, b"", destination, b"", request])

def MPloadBalancer(shape):

    # TODO: Binning de 2 pour Max Proj si > 4M
    """Load balancer for Maximum Projection
    """
    # Prepare context and sockets
    Log("MP Load Balancer Started")
    # frontend is connected to the GUI @ 5560
    # This should be with the IP
    context = zmq.Context.instance()
    frontend = context.socket(zmq.ROUTER)
    frontend.bind('tcp://*:5560')
    #backend is connected to the workers @ 5561
    backend = context.socket(zmq.ROUTER)
    backend.bind("tcp://*:5561")

    #workers_MP = []#["Worker-%i" for i in range(HF.ncpus)]
    #MP_count = 0
    poller = zmq.Poller()

    md = {'dtype': 'int32', 'shape': shape}
    MP = np.zeros(shape, dtype=np.int32)

    poller.register(backend, zmq.POLLIN)
    poller.register(frontend, zmq.POLLIN)

    # Use timer to send request to workers
    timers = ZmqTimerManager()
    timer = ZmqTimer(3, send_multi, backend, [], b"MaxProj-Client", b"MAXPROJ")
    timers.add_timer(timer)

    while True:

        timers.check()
        sockets = dict(poller.poll(timers.get_next_interval()))

        # Handle worker activity on the backend
        if backend in sockets:

            request = backend.recv_multipart()
            worker, message = request[:2]

            if message == b"START":
                timer.workers_MP.append(worker)
                #Log(worker)

            if message == b"resetMP":
                MP[::] = 0

            if message != b"READY" and len(request) > 2:
                #try:
                    # This is a message corresponding to a hit

                    packed = request[2]
                    #frontend.send_multipart([client, b"", packed])
                    unpacked = msgpack.unpackb(packed, object_hook=m.decode)
                    MP[:] = np.maximum(unpacked["MP"], MP)
                    Log("MP Load Balancer received MP from %s (max value = %4i)" % (str(worker), np.max(MP)))

                    #sjson = request[2]
                    #spots = json.loads(sjson)
                    #MP[spots['x'], spots['y']] = spots['I']
                #except:
                #    pass

        if frontend in sockets:
            Log("MP Load Balancer sending MP data to the GUI")
            client, _, request = frontend.recv_multipart()

            if request == b"STOP":
                frontend.send_multipart([client, b"", b"STOP"])
                break
            else:
                map_object = {'data': memoryview(MP),
                              'md': md}
                packed = msgpack.packb(map_object)
                frontend.send_multipart([client, b"", packed])


    # Clean up
    backend.close()
    frontend.close()
    context.term()
    Log("MP Load Balancer exited properly")


class StatsLoadBalancer(multiprocessing.Process):
    def __init__(self, wrk_num, setup, run_crystfel=True):
        super().__init__()
        self.wrk_num = wrk_num
        self.setup = setup
        self.run_crystfel = run_crystfel
        #self.zmq_crystfel = zmq_crystfel

    def setup_zmq(self):
        # Prepare context and sockets
        Log("Stats Load Balancer Started")
        # frontend is connected to the GUI @ 5550
        self.context = zmq.Context.instance()
        self.frontend = self.context.socket(zmq.ROUTER)
        self.frontend.bind('tcp://*:5550')
        # backend is connected to the workers @ 5551
        self.backend = self.context.socket(zmq.ROUTER)
        self.backend.bind("tcp://*:5551")

        self.mxcube = self.context.socket(zmq.PULL)
        self.mxcube.connect("tcp://0.0.0.0:5558")

        return

    def run(self):

        #if self.run_crystfel:# and not self.zmq_crystfel:
        self.CrystFELManager = CrystFELManager()
        self.CrystFELManager.start()

        self.setup_zmq()
        poller = zmq.Poller()
        #if self.zmq_crystfel:
        #    poller.register(self.crystfel, zmq.POLLIN)
        poller.register(self.backend, zmq.POLLIN)
        poller.register(self.frontend, zmq.POLLIN)
        poller.register(self.mxcube, zmq.POLLIN)

        #Add crystFEL to this.
        workers = []
        filenames = []
        file_number = []
        hits = []
        nPeaks = []
        total = 0
        Nhits = 0
        Nrun = 0
        processed = 0
        spots = []
        spots_results_x = []
        spots_results_y = []
        DC = {}
        SND_MXCUBE = False
        self.experimental_setup = {'number_images': 1}

        while True:
            sockets = dict(poller.poll())


            if self.mxcube in sockets:
                Log("Stats balancer Received message from mxcube")
                Nrun += 1
                total = 0
                # ToDo: Reinitialize other stats
                self.experimental_setup = self.mxcube.recv_json()
                SND_MXCUBE = True
                for worker in workers:
                    # Log("Sending message to %s" % str(worker))
                    self.backend.send(worker, flags=zmq.SNDMORE)
                    self.backend.send_json(self.experimental_setup)
                self.CrystFELManager.onThread(('new_DC', self.experimental_setup))




            # Handle worker activity on the backend
            # In this zmq socket, the first element of the message is its origin (identity of the sender, here Worker-i
            if self.backend in sockets:
                request = self.backend.recv_multipart()
                worker, message = request[:2]
                # Here prepare messages to be sent to GUI and crystFEL
                # TODO: Unpack the message and add it
                if message == b"START":
                    workers.append(worker)
                else:
                    results = msgpack.unpackb(message, object_hook=m.decode)
                    filenames.extend(results['filenames'])
                    file_number.extend(results['file_number'])
                    hits.extend(results['hits'])
                    nPeaks.extend(results['nPeaks'])
                    spots.extend(results["spots"])
                    self.CrystFELManager.onThread(('results', results))
                    processed += len(results['file_number'])
                    total += len(results['file_number'])
                    Nhits += sum(results['hits'])


            # frontend is the gui
            if self.frontend in sockets:
                # Get next client request
                client, _, request = self.frontend.recv_multipart()
                if request == b"STATS":
                    if SND_MXCUBE:
                        self.frontend.send_multipart([client, b"", msgpack.packb(self.experimental_setup)])
                        SND_MXCUBE = False

                    to_send = {"processed": processed,
                               "Nhits": Nhits,
                               "hits": hits,
                               "progress": float(total) / self.experimental_setup['number_images'] * 100,
                               "run": Nrun,
                               "nPeaks": nPeaks,
                               "file_number": file_number}
                    processed = 0
                    Nhits = 0
                    hits = []
                    nPeaks = []
                    filenames = []
                    file_number = []
                    spots = []
                    #Log("Total processed: %6i || Total: %6i || Progress: %4.2f" % (total, self.experimental_setup['number_images'],
                    #                                                               float(total) / self.experimental_setup['number_images'] * 100))
                    self.frontend.send_multipart([client, b"", msgpack.packb(to_send)])

                if request == b"STOP":
                    self.frontend.send_multipart([client, b"", request])
                    self.CrystFELManager.stopEvent.set()
                    Log("Stats Load Balancer: Stop message received")
                    break

        self.backend.close()
        self.frontend.close()
        self.mxcube.close()
        self.context.term()
        Log("Stats Load Balancer exited properly")

class CrystFELManager(threading.Thread):

    def __init__(self, timeout=0.1, auto_crystFEL=False):
        super().__init__()
        self.queue = queue.Queue()
        self.timeout = timeout
        self.stopEvent = threading.Event()
        self.lock = threading.Lock()
        self.spots_file = None
        self.hits_file = None
        self.hits_counter = 0
        self.tstart = time.time()
        self.port = 5002
        self.auto_crystFEL = auto_crystFEL


    def onThread(self, item):
        self.queue.put(item)

    def run(self):

        while True:
            if self.stopEvent.isSet():
                self.close_files()
                Log("Crystfel manager received killing signal: Exiting")
                return
            with self.lock:
                try:
                    message, item = self.queue.get(timeout=self.timeout)
                    if message == 'results':
                        self.write_results(item)
                    elif message == 'new_DC':
                        self.start_new_data_collection(item)#Log("CrystFEL Manager received spots from %i images" % len(spots))

                    #self.write(spots)
                except queue.Empty:
                    self.idle()


    def close_files(self):
        if self.spots_file is not None and not self.spots_file.closed:
            self.spots_file.close()
        if self.hits_file is not None and not self.hits_file.closed:
            self.hits_file.close()


    def start_new_data_collection(self, item):
        self.tstart = time.time()
        if self.hits_counter != 0:
            self.hits_counter = 0
            #self.start_new_CrystFEL_job()
        self.mxcube_dic = item
        self.spots_file_counter = 0
        self.crystfel_job_counter = 0
        self.hits_counter = 0
        self.fn_counter = 0

        self.proc_dir = os.path.join(self.mxcube_dic['fileinfo']['process_directory'], 'PF8')
        if not os.path.exists(self.proc_dir):
            os.makedirs(self.proc_dir)
        #if self.auto_crystFEL:
        #    if not os.path.exists(self.cryst_dir):
        #        os.mkdir(self.cryst_dir)
        #    self.cryst_dir = os.path.join(self.mxcube_dic['fileinfo']['process_directory'],'crystFEL')

        self.close_files()
        self.spots_file_fn = os.path.join(self.proc_dir,
                                          '%s_%04d.spots' % (self.mxcube_dic['fileinfo']['template'][:-9], self.spots_file_counter))
        self.spots_file = open(self.spots_file_fn, 'w')
        self.hits_file = open(os.path.join(self.mxcube_dic['fileinfo']['process_directory'],
                                           '%s.hits' % self.mxcube_dic['fileinfo']['template'][:-9]), 'w')

    def write_results(self, results):
        self.tstart = time.time()
        self.hits_file.write("\n".join([fn for idx, fn in enumerate(results["filenames"]) if results["hits"][idx]])+"\n")
        #spots, filenames = item
        for idx, spots in enumerate(results["spots"]):
            if results["hits"][idx] and self.mxcube_dic['fileinfo']['template'][:-9] in results['filenames'][idx][:-9]:
                self.spots_file.write(f"----- Begin chunk -----\n"
                                      f"Image filename: {results['filenames'][idx]}\n"
                                      f"Image serial number: {self.hits_counter}\n"
                                      f"num_peaks = {results['nPeaks'][idx]}\n"
                                      f"clen = {self.mxcube_dic['detector_distance']:8.4f}\n"
                                      f"Peaks form peak search\n"
                                      f"  fs/px   ss/px   Intensity\n"
                                      )
                for spot in spots:
                    #if spot["intensity"] < 65000:
                        self.spots_file.write(f"{spot['pos1']:8.2f}{spot['pos0']:8.2f}{spot['intensity']:12.2f}\n")
                self.spots_file.write(f"End of peak list\n"
                                      f"----- End chunk -----\n")
                self.hits_counter += 1

        if self.hits_counter >= 250 or time.time() - self.tstart > 60:
            #if self.auto_crystFEL:
            #    self.start_new_CrystFEL_job()
            self.tstart = time.time()
            self.hits_counter = 0
            self.spots_file.close()
            self.create_new_spotfile()

    def start_new_CrystFEL_job(self):
        params = { 'spot_list': self.spots_file_fn,
                   'geom': 'pilatus2M_id232.geom',
                   'stream': os.path.join(self.cryst_dir,
                                          self.mxcube_dic['fileinfo']['template'][:-9]+'_%04d.stream' % self.crystfel_job_counter),
                   'cell': 0,
                   'port': str(self.port)
        }
        if self.hits_counter > 0:
            with ServerProxy("http://160.103.228.110:8080") as proxy:
                try:
                    proxy.start_crystFEL(params)
                except ConnectionRefusedError as err:
                    print("Error")

            Log(f"Started CrystFEL job # {self.crystfel_job_counter}")
            self.crystfel_job_counter += 1
            self.port += 1

    def create_new_spotfile(self):
            self.spots_file_counter += 1
            self.spots_file_fn = os.path.join(self.proc_dir,
                                              '%s_%04d.spots' % (self.mxcube_dic['fileinfo']['template'][:-9], self.spots_file_counter))
            self.spots_file = open(self.spots_file_fn, 'w')

    def idle(self):
        if time.time() - self.tstart > 60 and self.hits_counter != 0:
            self.tstart = time.time()
            self.hits_counter = 0
            #if self.auto_crystFEL:
            #    self.start_new_CrystFEL_job()
            if not self.hits_file.closed:
                self.hits_file.close()
            if not self.spots_file.closed:
                self.spots_file.close()
        #time.sleep(0.1)


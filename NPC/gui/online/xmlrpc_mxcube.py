import sys
import threading
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
import zmq
import time
import os


class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

class Ventilator(threading.Thread):

    def __init__(self):
        super().__init__()
        self.context = zmq.Context()
        self.lock = threading.Lock()
        self.mxcube_dic = None
        self._stop_ventilating = True
        self._stop = threading.Event()
        # Set up a channel to send work
        self.ventilator_send = self.context.socket(zmq.PUSH)
        self.ventilator_send.bind("tcp://*:9999")
        time.sleep(1)

    def run(self):
        while True:
            if self._stop.isSet():
                print("Ventilator received killing signal: Exiting")
                return
            with self.lock:
                if not self._stop_ventilating:
                    self.num = self.mxcube_dic['image_first']
                    while self.num <= self.mxcube_dic['number_images']:
                        if not self._stop_ventilating and not self._stop.isSet():
                            fn = os.path.join(self.mxcube_dic['fileinfo']['directory'],self.mxcube_dic['fileinfo']['template']) % self.num
                            work_message = {'filename': fn, 'file_number': self.num}
                            if os.path.isfile(fn):
                                #To be uncommented
                                self.ventilator_send.send_json(work_message)
                                #print("Sending message to workers: %s" % work_message['filename'])
                                self.num += 1
                            else:
                               time.sleep(0.01)
                        else:
                            break
                    print("Ventilated %i images" % (self.num-1))
                    self._stop_ventilating = True
                else:
                    time.sleep(1)


quit = 0
def kill():
    global quit
    quit = 1
    return 1

def start_server(pilatus=True):

    context = zmq.Context()
    mxcube_push = context.socket(zmq.PUSH)
    mxcube_push.bind("tcp://*:5558")

    if pilatus:
        thread = Ventilator()
        thread.start()

    def start(mxcube):
        if pilatus:
            if not thread._stop_ventilating:  # Thread is still ventilating images
                thread._stop_ventilating = True
            time.sleep(0.1)
            thread.mxcube_dic = mxcube
            thread._stop_ventilating = False
        #print("Sending mxcube info to GUI")
        mxcube_push.send_json(mxcube)
        return True

    def stop():
        #if pilatus:
        #    thread._stop_ventilating = True
        return True

    with SimpleXMLRPCServer(("localhost", 9998)) as server:
        server.register_function(start)
        server.register_function(stop)
        server.register_function(kill)
        while not quit:
            server.handle_request()
        if pilatus:
          thread._stop.set()
        sys.exit(0)

if __name__ == '__main__':
    start_server()

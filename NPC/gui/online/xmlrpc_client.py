from xmlrpc.client import ServerProxy

import sys
import time

try:
    t = int(sys.argv[1])
except:
    t = None

mxcube_dic = {'exposure': 0.006,
              'detector_distance': 229.660,
              'wavelength': 0.847,
              'orgx': 755.51 * 0.172,
              'orgy': 825.98 * 0.172,
              'oscillation_range': 0.100,
              'start_angle': 90.003,
              'number_images': 6000,
              'image_first': 1,
              'experiment_type': 'Mesh',
              'number_of_lines': 60,
              'fileinfo': {
                  'directory': '/data/visitor/mx2328/id23eh2/20210916/RAW_DATA/ocp_mut_dark/ocp_mut_dark-ocpmut_batch1_darK_chip1/MeshScan_10',
                  'template': 'mesh-ocpmut_batch1_darK_chip1-ocp_mut_dark_0_%04d.cbf',
                  'process_directory': '/data/visitor/mx2328/id23eh2/20210916/PROCESSED_DATA/ocp_mut_dark/ocp_mut_dark-ocpmut_batch1_darK_chip1/MeshScan_10'

                       }              
              }

with ServerProxy("http://localhost:9998") as proxy:
        try:
            proxy.start(mxcube_dic)
        except ConnectionRefusedError as err:
            print("Error")


if t is not None:
    time.sleep(t)
    proxy.stop()

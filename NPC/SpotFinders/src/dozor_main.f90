PROGRAM dozor
    !                   ==============
    !______________________________________________________________________
    !______________________________________________________________________
    ! Written by A.Popov and G.Bourenkov
    !
    !!                 +-------------------------------------+
    !!                 |   D E S C R I P T I V E   P A R T   |
    !!                 +-------------------------------------+

    use generic_data_plugin, ONLY: library, nx, ny, &
    generic_open, generic_get_header, generic_get_data, generic_close
    implicit none
    include 'dozor_param.fi'  
    include 'dozor_dimension.fi'  
    type (DETECTOR)   D       ! DETECTOR parameters
    type (DATACOL)    X, Xin  ! D.C.    parameters 
    type (DATACOL_PICKLE), allocatable :: Xp(:)  ! intermediates for omp
    type (DATACOL_PICKLE) Xp_single
    type (LOCAL)      LC      ! local parameters  
    integer i, imnum
    character*1024 templ, path
    integer*1, allocatable :: PSIim(:),KLim(:)  
    integer*2, allocatable :: image2(:)
    integer*4, allocatable :: image4(:)
!----------------
    integer             :: ierr, nbyte, generic_info_array(1024), number_of_frames_unused
    real                :: qx_unused, qy_unused
    character(len=:), allocatable :: master_file_path

!------------------------------------------------------------------
    call dozor_print_version() 
    call dozor_tellhelp()     
    call dozor_set_defaults(X)
    call dozor_input(X,D,templ,path)
    library=path(1:lnblnk(path))
    allocate ( PSIim(D%ix_unbinned*D%iy_unbinned) ) 
    allocate ( KLim (D%ix_unbinned*D%iy_unbinned) ) 
    call pre_dozor(D, X, LC, PSIim, KLim, 0)
    call make_master_file_path(templ, path, X%image_first)
    master_file_path=path(1:lnblnk(path))
    call generic_open(library, master_file_path, generic_info_array, ierr)
    if (ierr < 0 ) call exit(-1)
    call generic_get_header(nx, ny, nbyte, qx_unused, qy_unused, & 
                            number_of_frames_unused, generic_info_array, ierr)
    if (ierr < 0 ) call exit(-1)
    call check_image_size(nx, ny, D)
    allocate(Xp(X%number_images))
    Xin=X
    allocate(image4(D%ix_unbinned*D%iy_unbinned))
    allocate(image2(D%ix*D%iy))
!$omp parallel do firstprivate(X) &
!$omp& private(i, image2, image4, Xp_single, ierr, imnum) &
!$omp& shared(PSIim, KLim, Xp, D, LC, Xin, generic_info_array)
    do i=0, X%number_images - 1
        imnum = X%image_first + i
        call generic_get_data(imnum, D%ix_unbinned, D%iy_unbinned, image4, generic_info_array, ierr)
        if(ierr .ge. 0)then
           call dozor_bin_image(D, image4, 4, image2)
           call dozor_do_image(image2, D, Xin, X, Xp_single, LC, PSIim, KLim)
           call write_spot_list(X, templ, imnum, Xp_single%NofR, D%binning_factor)
           call write_spot_wilson_mtv(X, Xp_single, imnum, D%ix, D%iy, D%binning_factor)
       endif
       Xp(i+1)=Xp_single

    enddo 
    deallocate(image4)
    deallocate(image2)
    call results_print(Xin, Xp)
    call results_print_average(Xin, Xp)
    if(Xin%backg) call background_print(D, Xin, Xp, LC)
    if(Xin%isum) call write_sum_int(X, Xp)
    deallocate(PSIim)
    deallocate(KLim)
    deallocate(Xp)
    STOP
    END

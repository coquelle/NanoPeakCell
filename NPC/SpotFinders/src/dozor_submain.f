      subroutine check_image_size(nx,ny,D)
      implicit none
      integer nx,ny
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'
      type (DETECTOR) D
      if (nx.ne.D%ix_unbinned .or. ny.ne.D%iy_unbinned) then
          write(0,*)'ERROR: image size from image header',nx,ny, 
     +       ' mismatches that in the input file' 
          call exit(-1)
      endif
      return
      end

      subroutine make_master_file_path(templ, master_file, first)
      implicit none
      character*1024 templ
      character*(*) master_file
      integer first
      integer lmax
c 
      lmax=len_trim(templ)
      if (templ(lmax-2:lmax) == '.h5') then
         master_file= templ(:lmax-9)//'master.h5'
      else if (templ(lmax-3:lmax) == '.cbf' .or. 
     +         templ(lmax-6:lmax) == '.cbf.gz') then
        call template_substitute_frame_number(templ, master_file, first)       
      else
         write(0,*)'ERROR: template must have .h5, .cbf'//
     +      ' or .cbf.gz extension'
         call exit(-1)
      endif
      return
      end

      subroutine template_substitute_frame_number(templ, path, number)
      implicit none
      character*1024 templ
      character*(*) path
      integer number
      character*4 fmt 
      integer firstqm, lastqm

      firstqm = index(templ,'?')
      if(firstqm.eq.0)then
         write(0,*) "ERROR: template contains no '?' character" 
         call exit(-1)
      endif
      lastqm =  index(templ,'?', .true. )
      fmt="I . "
      write(fmt(2:2),'(i1)')lastqm-firstqm+1
      write(fmt(4:4),'(i1)')lastqm-firstqm+1
      path=templ(1:lnblnk(templ))
      write(path(firstqm:lastqm),"("//fmt//")") number
      return
      end

      subroutine dozor_input(X,D,templ,library)
      implicit none
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'
      type (DETECTOR) D
      type (DATACOL)  X
      character*1024 templ
      character*(*) library
      integer first
      logical fail
      character*1024 string
c
      first=1
      call read_demand(X,first)    ! read command line
      call iscancl("-bin",D%binning_factor,fail)
      if(fail)then
          D%binning_factor=1
      else
          first =first+2
      endif 
      call getarg(first,string)    ! read the file name where all data
      call read_dozor(D,X,string,templ,library)
      if(X%number_images.gt.nimage)then
         write(0,*) 'ERROR: Number of images can not be exceed',nimage
         call exit(-1)
      endif
      return
      end


      subroutine dozor_print_version()
      implicit none
      WRITE(UNIT=6,fmt='(a,/,a,/,a)')
     +  " Program dozor /A.Popov & G.Bourenkov/",
     +  " Version 2.0.2 //  20.03.2020",
     +  " Copyright 2014-2020 by Alexander Popov and Gleb Bourenkov"
      return
      end


      subroutine dozor_get_spot_list(D,X,Xp,ref_list)
      implicit none
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'
      type (DETECTOR) D
      type (DATACOL)  X
      type (DATACOL_PICKLE) Xp
      type (nice_reflection) ref_list(Xp%NofR)
      integer i, kl, j
      i=0
      do kl=0,nrbin
         do j=1,INT(X%RList(kl,0))
            i=i+1
            if (i .gt. Xp%NofR) return ! should never happen, but... 
            ref_list(i)%x=D%binning_factor*(REAL(X%hklKoor(kl,j,1))-1.0)
            ref_list(i)%y=D%binning_factor*(REAL(X%hklKoor(kl,j,2))-1.0)
            ref_list(i)%intensity=X%Rlist(kl,j)
         enddo
      enddo
      return
      end

      subroutine make_beamstop_shadow(D,X)
      implicit none
      include 'dozor_param.fi'
      type (DETECTOR) D
      type (DATACOL) X 
c
      real delta
c
      delta = 0.5 * X%beamstop_size * X%dist / X%beamstop_distance 
      if ( X%beamstop_vertical ) then
          X%kxmin = int(X%xcen - delta) 
          X%kxmax = int(X%xcen + delta)
          X%kymin = X%ycen - 10
          X%kymax = D%iy
      else
          X%kxmin = 0
          X%kxmax = X%xcen - 10
          X%kymin = int(X%ycen - delta)
          X%kymax = int(X%ycen + delta)
      endif
      return
      end


      subroutine dozor_bin_image(D, unbinned, size, binned)
      implicit none
      include 'dozor_param.fi' 
      type (DETECTOR) D
      integer size
      integer unbinned(1)
      integer binned(1)
      if (size.eq.2) then
         call bin2(D,unbinned,binned)
      else
         call bin4(D,unbinned,binned)
      endif
      return
      end

      subroutine bin4(D,unbinned,binned)
      implicit none
      include 'dozor_param.fi'
      type (DETECTOR) D
      integer*4  unbinned(0:D%ix_unbinned-1,0:D%iy_unbinned-1)
c
      integer*2 binned(0:D%ix-1,0:D%iy-1)
      integer value
      integer i,j
      integer iu, ju
      integer ifrom, ito, jfrom, jto
      if(D%binning_factor .le. 1) then
         binned = unbinned
         return
      endif
      binned=0
      do j=0,D%iy-1
         jfrom = j*D%binning_factor
         jto = (j+1)*D%binning_factor-1
         ifrom = 0
         do i=0,D%ix-1
            ifrom = i*D%binning_factor
            ito = ifrom + D%binning_factor - 1
            value = 0
            do ju=jfrom, jto
               do iu =ifrom, ito
                  if (unbinned(iu,ju).lt.0) then
                     binned(i,j)= -1 !unbinned(iu,ju)
                     goto 1
                  else
                     value = value + unbinned(iu,ju)
                  endif
                enddo
            enddo
            if (value .lt. 32767) then
               binned(i,j)=value
            else
               binned(i,j)=32767
            endif
    1       continue
         enddo
      enddo
      return
      end

      subroutine bin2(D,unbinned,binned)
      implicit none
      include 'dozor_param.fi'
      type (DETECTOR) D
      integer*2  unbinned(0:D%ix_unbinned-1,0:D%iy_unbinned-1)
c
      integer*2 binned(0:D%ix-1,0:D%iy-1)
      integer value
      integer i,j
      integer iu, ju
      integer ifrom, ito, jfrom, jto
      if(D%binning_factor .le. 1) then
         binned = unbinned
         return
      endif
      binned=0
      do j=0,D%iy-1
         jfrom = j*D%binning_factor
         jto = (j+1)*D%binning_factor-1
         ifrom = 0
         do i=0,D%ix-1
            ifrom = i*D%binning_factor
            ito = ifrom + D%binning_factor - 1
            value = 0
            do ju=jfrom, jto
               do iu =ifrom, ito
                  if (unbinned(iu,ju).lt.0) then
                     binned(i,j)= -1 !unbinned(iu,ju)
                     goto 1
                  else
                     value = value + unbinned(iu,ju)
                  endif
                enddo
            enddo
            if (value .lt. 32767) then
               binned(i,j)=value
            else
               binned(i,j)=32767
            endif
    1       continue
         enddo
      enddo
      return
      end

      subroutine dozor_do_image(img,D,Xin,X,Xp,LC,PSIim,KLim)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL) Xin, X
      type (DETECTOR) D
      type (DATACOL_PICKLE) Xp
      type (LOCAL) LC
      integer*2 img(D%ix*D%iy)
      integer*1 PSIim(D%ix*D%iy), KLim(D%ix,D%iy)
c
c      open(1,file='/tmp/junk',form='unformatted',
c     +      access='direct',recl=D%ix*D%iy*2)
c      write(1,rec=1)img
c      close(1)

      X=Xin

      call hkl_direct(img,D,X,Xp,LC,PSIim,KLim)
      call make_scores(X,Xp)
      return
      end

      subroutine dozor_set_defaults(X)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL) X
c      type (DETECTOR) D
c
      call init_datacol(X)
c
      X%texposure = 0.010;
      X%nbad = 0;
      X%wedge = 1;
      X%phiwidth = 0.1;
      X%start_angl = 0.;
      X%number_images = 1;
      X%image_first = 1;
      X%pixel_min = 0;
      X%pixel_max = 32000;
      X%monoch=0.99
c
      X%graph = 0;
      X%sprint = 0;
      X%backg = 0;
      X%w = 0;
      X%wg = 0;
      X%rd = 0;
      X%isum = 0;
      X%prAll = 0;
c
      X%Ispot = 3;
c     
      X%beamstop_size = .5
      X%beamstop_distance = 10.0  
      X%beamstop_vertical = .true.
c
      X%sigLev = 6.0      
c
      return
      end

      subroutine prepare_vbina_vbins(X)
      implicit none
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'
      type (DATACOL) X
      integer i
      real ht1,ht2
c
      X%vbins(0)=0
c
      do i=1,nrbin
         ht1=Sqrt(X%hmin2+(i-1)*X%delh2)
         ht2=Sqrt(X%hmin2+i*X%delh2)
         X%vbina(i)=ht2**3-ht1**3
         X%vbins(i)=X%vbins(i-1)+X%vbina(i)
      enddo
      return
      end

      subroutine make_scores(X,Xp)
      implicit none
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'
      type (DATACOL) X
      type (DATACOL_PICKLE) Xp ! single frame in this routine
c
      integer i,j
c
      if(Xp%dlim.ne.0)then
          j=1
           do i=1,nrbin
               if((X%hmin2+i*X%delh2).le.(1/Xp%dlim)**2)j=i
           enddo
           Xp%Score2=Xp%Iav*(1.-Xp%Rfexp)**2
     +           *X%vbins(j)/X%vbins(nrbin)
           Xp%Score3=Xp%Score2*0.01
      else
           Xp%Score2=0
      endif
      if(Xp%table_suc)then
         Xp%table_est=Xp%table_intsum
c	 *Xp%table_corr*(1.-Xp%table_rfact)
	 
         j=1
         do i=1,nrbin
            if((X%hmin2+i*X%delh2).le.(1/Xp%table_resol)**2)j=i
         enddo
         Xp%table_est=Xp%table_est * X%vbins(j)/X%vbins(nrbin)
	 
         Xp%Score3=(Xp%table_est+Xp%Score2)*0.5
     +      *(1-((Xp%table_est-Xp%Score2)/(Xp%table_est+Xp%Score2))**2)
     
c          Xp%Score3=Xp%table_est*Xp%table_corr*(1.-Xp%table_rfact)
      endif
      return
      end

      subroutine pre_dozor(D,X,LC,PSIim,KLim, DEBUG)
      implicit none
      integer DEBUG
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'
      type (DETECTOR) D
      type (DATACOL) X
      type (LOCAL) LC
      integer*1 PSIim(D%ix*D%iy), KLim(D%ix*D%iy)
c
      integer ixcen, iycen
      real rdetmax
      real hmax
c
      call prepare_detector_binning(D,X)

      X%dist=X%dist/D%pixel !!!! beware of possible recursion !!!!
      X%aconst=868.0*(1.5418/X%wave)**(2.73)
      ixcen=X%xcen+0.5
      iycen=X%ycen+0.5
      rdetmax= MIN0(ixcen,iycen,(D%iy-iycen),(D%ix-ixcen))
      rdetmax=rdetmax-10
      hmax =(2.*SIN(0.5*ATAN(rdetmax/X%dist)))/X%wave
      X%hmax2=AMIN1(hmax**2,2.8725) ! according to max resolution wilson curve
      X%hmin2=hmin**2
      X%delh2=(X%hmax2-X%hmin2)/nrbin
      X%mrd = 1
      X%mgain=1

      if ( X%Kxmin+X%Kymin+X%Kxmax+X%Kymax .le. 0 ) 
     +                          call make_beamstop_shadow(D,X)
      call psikl_prep(D, X, LC, PSIim, KLim)
      call factorial_table(X)
      call prepare_vbina_vbins(X)
c
      if(DEBUG.eq.5) then
         open(1,file='/tmp/dozor_DEBUG')
         write(1,*)X
         close(1)
      endif
      return
      end 

      subroutine printx(X)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL)    X
      open(1,file='X')
         write(1,*)X
      close(1)
      return
      end
c
      subroutine printxp(Xp)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL_PICKLE)    Xp
      open(1,file='Xp')
         write(1,*)Xp
      close(1)
      return
      end

      subroutine printlc(LC)
      implicit none
      include 'dozor_param.fi'
      type (LOCAL)    LC
      open(1,file='LC')
         write(1,*)LC
      close(1)
      return
      end

c==================================================================
	  	

      subroutine factorial_table(X)
      implicit none
      real bt, ert
      real factor
      integer i,l
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'
      type (DATACOL) X 
      bt=-0.01
      i=-1
      do while(bt.le.11)
         i=i+1
         bt=bt+0.01
         l=Int(bt)
         ert=Exp(-bt)*bt**l/factor(l)                
         do while (ert.ge.0.005)
            l=l+1
            ert=Exp(-bt)*bt**l/factor(l)
         enddo
         X%pLim1(i)=l
                
         do while (ert.ge.1e-6)
            l=l+1
            ert=Exp(-bt)*bt**l/factor(l)
         enddo                
         X%pLim2(i)=l                 
       enddo
       return
       end

c-begin-text ICE_FIND

         SUBROUTINE ice_find(X,redus)
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c  find ice  using 2 templates from background.fi  
c ******************************************************************

!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      IMPLICIT NONE 
      include 'dozor_param.fi' 
           
      include 'dozor_dimension.fi'         
      type (DATACOL) X 
      integer i,j,ni,k,ii,ibest     
      
      real*8 cof_sum,cc,cof1,cof2,cct,dt   
      real  sigma,sigma_min
      logical redus(nrbin) 
      integer nsig
      real B,Bb
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+

          sigma_min=1e10
          nsig=5
           do i=1,3

	    call fit_backrgound(X,i,sigma,redus,cof_sum,cc,nsig)
	    if(sigma.le.sigma_min)then
	        ibest=i
		sigma_min=sigma
		                  endif		  
           enddo 
c	   pause
c	    ibest=1
	    nsig=4
	    call fit_backrgound(X,ibest,sigma,redus,cof_sum,cc,nsig)    
	
10       continue	
		        			     				     	       	  	
         return
          end 
	  


	  	
        subroutine  PSIKL_prep(D,X,LC,PSIim,KLim)
	
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
!!   prepare tables of PSI and Resolution
!     and  polarisation factors and absorbtion for 2D- bins
!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      implicit none
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'      
      type (DETECTOR) D
c      type (CRYSTAL)  C
      type (DATACOL)  X
      type (LOCAL)    LC      ! local parameters  
              
      integer*4 imsize      
      integer*1 PSIim(D%ix*D%iy),KLim(D%ix*D%iy)  
      REAL xcen,ycen
      INTEGER ixcen,iycen 
      REAL dist2,radu,radu2,Rr,Rr2,sintet,htem2,h2t 
      REAL rdetmax,rdetmin      
      INTEGER i,j,kx,ky
      REAL dpsi      
      INTEGER*4 koor,koormax
      REAL yt,yt2,xt,xt2,h,h2,psi,ht
      REAL ytr,xtr      
      INTEGER kl,klpsi  
      integer itt,kxt,kyt,j1,jm 
      character*1 rotd !if detector x (fast axis) = rotaxis then = x, else = y 
      REAL*8  sin2tet  
      real p0,monsin2 
       
      REAL idealb_beamline(150),h2back(150)
      REAL idealback1(150),idealback2(150),idealback3(150)     
      
      include 'background_id.fi' 
      REAL b0,b1,b2,b3,sb0,sb1,sb2,sb3,h2t_min,h2t_max,d_min      
                              
c       CONSTANTs calculation
c =================================================
      rotd = 'x'

      koormax=D%ix*D%iy
      imsize=koormax 
      xcen=X%xcen
      ycen=X%ycen      
      ixcen=X%xcen+0.5
      iycen=X%ycen+0.5
      dist2=X%dist**2

      rdetmin=X%dist*TAN(2*ASIN(hmin*X%wave*0.5))
      
	rdetmax	= MIN0(ixcen,iycen,(D%iy-iycen),(D%ix-ixcen))

      radu =X%dist*TAN(2.*ASIN(0.5*Sqrt(X%hmax2)*X%wave))
c      if(rdetmax.gt.radu)rdetmax=radu

c relative bin volume      
              ht=Sqrt(X%hmin2)
              X%vbin(1)=1
          do i=1,nrbin
              h2t=Sqrt(X%hmin2+i*X%delh2)
              X%vbin(i)=(h2t**3-ht**3)/X%vbin(1)
	      ht=h2t
          enddo      
             X%vbin(1)=1

C**** step of psi
c     ===================
      dpsi=pi/(0.5*npsibin)
      
c     tables PSI and KL
          do ky=1,D%iy,1
            do kx=1,D%ix,1
           koor=kx +(ky-1)*D%ix  
	    PSIim(koor)=-1
	    KLim(koor)=0
	    enddo
	   enddo 
	 	
c___________________________________________________________________

         do ky=1,D%iy,1	 
            yt=abs(ky-ycen)
	                
            do kx=1,D%ix,1	    
               xt=abs(kx-xcen)       
               yt2=yt**2	       
               xt2=xt**2	       	            
               radu2=(xt2+yt2)
               radu=sqrt(radu2)
               if(radu.lt.rdetmax)then
c ==========  bins of resolution

                  Rr2=dist2+radu2
                  Rr=sqrt(Rr2)
                  sintet=SQRT(0.5*(1.0000-X%dist/Rr))
                  h=(2.000*sintet)/X%wave
                  h2=h**2
		  
                  kl=INT((h2-X%hmin2)/X%delh2)+1		  
                  if(kl.gt.nrbin)kl=nrbin
                  if((radu.lt.rdetmin))kl=0
		  
c     =========== bin of psi
                  if(rotd.eq.'x')psi=ACOS(xt/radu)
                  if(rotd.eq.'y')psi=ACOS(yt/radu)
                  klpsi=psi/dpsi+1
		  if(klpsi.gt.(npsibin/4))klpsi=npsibin/4
c
		     i=0
		     j=0
		     kxt=kx
		     kyt=ky-1
                        koor=kxt +
     +                       kyt*D%ix     
     
                xtr=kxt-xcen	    
                ytr=kyt-ycen	
		   
       if(koor.gt.0.and.koor.le.koormax)then
	 if(ytr.gt.0.and.xtr.gt.0)PSIim(koor)=klpsi
	 if(ytr.lt.0.and.xtr.gt.0)PSIim(koor)=klpsi+(npsibin/4)
	 if(ytr.lt.0.and.xtr.lt.0)PSIim(koor)=klpsi+(npsibin/2)	 
	 if(ytr.gt.0.and.xtr.lt.0)PSIim(koor)=klpsi+(3*npsibin/4)	

 	    KLim(koor)=kl  
	    
       if((kxt-1).ge.X%Kxmin.and.(kxt-1).le.X%Kxmax.and.
     +	(kyt-1).ge.X%Kymin.and.(kyt-1).le.X%Kymax)then
                KLim(koor)=-1
                PSIim(koor)=-1
			                          endif	
						
		 do j1=1,X%nbad		
       if((kxt-1).ge.X%Bxmin(j1).and.(kxt-1).le.X%Bxmax(j1).and.
     +	(kyt-1).ge.X%Bymin(j1).and.(kyt-1).le.X%Bymax(j1))then
                KLim(koor)=-1
                PSIim(koor)=-1

			                                 endif
		  enddo											    	     
		                        endif
			
		                             endif			     		          
               enddo
		 enddo 
		 
c 1)callculation of polarisation factors and absorbtion for 2D- bins
c ====================================================================

      do i=0,nrbin
         htem2=(X%hmin2+i*X%delh2)
         h2t=htem2-X%delh2*0.50000	 
         if(i.eq.0)h2t=htem2	 
c SIN(2teta) callcul. 
c ====================
         sin2tet=0.5*X%wave*SQRT(h2t)*
     +           SQRT(4-X%wave**2*h2t)
         LC%cos2tet2(i)=1.-sin2tet**2
    
c Polarisation
c ============
         p0=1-0.5*(1.-X%monoch)* sin2tet**2
         monsin2= sin2tet**2*X%monoch*0.5
               
         do j=1,npsibin/4
	    j1=npsibin/4-j+1 
            LC%pol(i,j)=p0- monsin2*(1+COS(dpsi*(2*j-1)))
    
         enddo

c Absorbtion
c ==========
         LC%absorb(i)=EXP(-X%dist*D%pixel/(SQRT(1-sin2tet**2)*X%aconst))
	  	 
      enddo
      
c-------------------------------------------------------------------
c    Prapere ideal backgrounds 
       do i=1,nrbin
	 
	  sb0=1	 
	  sb1=0
	 
	  h2t=X%hmin2+X%delh2*(i-1)	 
	  h2t_min=X%hmin2+X%delh2*nrbin
	
	  do j=1,150
	    
	     if(ABS(h2t-h2back(j)).le.h2t_min)then 
	        h2t_min=ABS(h2t-h2back(j))
	        sb0=j
	        if((h2t-h2back(j)).gt.0)then
	           sb1=1
	        else
	           sb1=-1
	        endif
	        if(abs(h2t-h2back(j)).le.0.002)sb1=0
	      endif
	  enddo	 
	  if(sb0+sb1 .gt. 150)sb1=0 
        X%idealback0(i)=
     +	0.5*(idealb_beamline(sb0)+idealb_beamline(sb0+sb1))
        X%idealback(i,1)=0.5*(idealback1(sb0)+idealback1(sb0+sb1))	
        X%idealback(i,2)=0.5*(idealback2(sb0)+idealback2(sb0+sb1))
        X%idealback(i,3)=0.5*(idealback3(sb0)+idealback3(sb0+sb1))	
		 
			 
	enddo

	         return
          end 	      		      

c-begin-text make_name

       subroutine make_name(
     &                    X,Outf,numb,
     &                    l1,l2,
     &                    namet)

     
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c   make spot_file and graph_name 
c ******************************************************************
c ******************************************************************

!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      IMPLICIT NONE 
      include 'dozor_param.fi'        
      type (DATACOL)    X       ! D.C.    parameters  
      type (OUTPUTFILES) Outf
           
      CHARACTER*1024 namet      
      INTEGER numb
      INTEGER k,j,l1,l2,l3

!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+               		  
	  
		 k=0
	  do while(numb.ge.10**k)
	      k=k+1
	  enddo

	     do j=l1,(l2-k)
		namet(j:j)= '0' 
	     enddo

	            l3=l2-k+1			
	if(k.eq.1) write(namet(l3:l2),FMT='(i1)')numb
	if(k.eq.2) write(namet(l3:l2),FMT='(i2)')numb	
	if(k.eq.3) write(namet(l3:l2),FMT='(i3)')numb
	if(k.eq.4) write(namet(l3:l2),FMT='(i4)')numb	
	if(k.eq.5) write(namet(l3:l2),FMT='(i5)')numb

ccccc --- spot file name
                   if(X%sprint)then
	   
	      do j=1,(5-k)
		Outf%spot_name(j:j)='0' 
             enddo
		
	            l3=5-k+1			
	if(k.eq.1) write(Outf%spot_name(l3:5),FMT='(i1)')numb
	if(k.eq.2) write(Outf%spot_name(l3:5),FMT='(i2)')numb	
	if(k.eq.3) write(Outf%spot_name(l3:5),FMT='(i3)')numb
	if(k.eq.4) write(Outf%spot_name(l3:5),FMT='(i4)')numb	
	if(k.eq.5) write(Outf%spot_name(l3:5),FMT='(i5)')numb
	     Outf%spot_name(6:10)='.spot' 
	                endif
			
           if(X%graph)then
	   
	      do j=1,(5-k)
		Outf%sgraph_name(j:j)='0' 
	      enddo
		
	    l3=5-k+1			
	if(k.eq.1) write(Outf%sgraph_name(l3:5),FMT='(i1)')numb
	if(k.eq.2) write(Outf%sgraph_name(l3:5),FMT='(i2)')numb	
	if(k.eq.3) write(Outf%sgraph_name(l3:5),FMT='(i3)')numb
	if(k.eq.4) write(Outf%sgraph_name(l3:5),FMT='(i4)')numb	
	if(k.eq.5) write(Outf%sgraph_name(l3:5),FMT='(i5)')numb
	     Outf%sgraph_name(6:10)='_spot' 
	     Outf%sgraph_name(11:14)='.mtv' 						
			
		do j=1,(5-k)
		Outf%wgraph_name(j:j)='0' 
		enddo
		
	    l3=5-k+1			
	if(k.eq.1) write(Outf%wgraph_name(l3:5),FMT='(i1)')numb
	if(k.eq.2) write(Outf%wgraph_name(l3:5),FMT='(i2)')numb	
	if(k.eq.3) write(Outf%wgraph_name(l3:5),FMT='(i3)')numb
	if(k.eq.4) write(Outf%wgraph_name(l3:5),FMT='(i4)')numb	
	if(k.eq.5) write(Outf%wgraph_name(l3:5),FMT='(i5)')numb
	Outf%wgraph_name(6:12)='_wilson' 
	Outf%wgraph_name(13:16)='.mtv' 	

	                endif		
cc                -------------			
			
         return
          end	



	   
c-begin-text FIT_BACKGROUND

         SUBROUTINE fit_backrgound
     +(X,current,sigma,redus,cof_sum,cc,nsig)
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c  find ice  using 2 templates from background.fi  
c ******************************************************************

!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      IMPLICIT NONE 
      include 'dozor_param.fi'            
      include 'dozor_dimension.fi'         
      type (DATACOL) X 
      integer current
      integer i,j,ni,k,ii 
      real*8 sum1,sum2,sum3,s1,s2   
        
      REAL dideal(nrbin),b1(nrbin),b2(nrbin)    
      
      real*8 cof_sum,cc,cof1,cof2,cct,dt   
      real  deltmin,h2t ,tt,sigma
      logical redus(nrbin) ,redusAll
      integer nsig
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+

	        do i=1,nrbin
		 redus(i)=.TRUE.
	        enddo
		
	        do i=1,nrbin
		 b1(i)=X%idealback0(i)
		 b2(i)=X%idealback(i,current)		 
	        enddo		

	       do k=1,3
	       	
	s1=0
	s2=0
	sum1=0
	sum2=0
	sum3=0
	        do i=1,nrbin
		if(redus(i))then	
	  dideal(i)=b1(i)-b2(i)
	  s1=s1+X%backpol(i)*dideal(i)
	  s2=s2+X%backpol(i)*b2(i)	  	
	  sum1=sum1+dideal(i)**2
	  sum2=sum2+dideal(i)*b2(i)
	  sum3=sum3+b2(i)**2	
	                  endif  
	       enddo	
	       
	   dt=1e10    
	      do j=0,100
	  cct=j*0.01 
	  cof1=(cct*s1+s2)/(sum1*cct**2+2*cct*sum2+sum3)
	  cof2=s1/(cct*sum1+sum2)
	   if(dt.ge.ABS(cof1-cof2))then
	      dt=ABS(cof1-cof2)	      
	   cof_sum=(cof1+cof2)/2
	   cc=cct
	                          endif
	     enddo
	     
c sigma estimate	     
	     	sum1=0
		ni=0
		
	        do i=1,nrbin
     	
		if(redus(i))then			     
	           sum1=sum1+(X%backpol(i)-
     &cof_sum*(cc*b1(i)+(1-cc)*b2(i)))**2
            ni=ni+1 
	                  endif  
		enddo
		sigma=SQRT(sum1/ni)
		redusAll=.TRUE.
	        do i=1,nrbin		     
       if((X%backpol(i)-
     &cof_sum*(cc*b1(i)+(1-cc)*b2(i))).le.
     &nsig*sigma)then
       redus(i)=.TRUE.
       else
       redus(i)=.FALSE.
       redusAll=.FALSE.
       endif                
                 enddo
	if(redusAll)exit	 	 
           enddo  		
		
         return
          end 


      subroutine prepare_detector_binning(D,X)
      implicit none
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'

      type (DETECTOR)   D       ! DETECTOR parameters
      type (DATACOL)    X       ! D.C.    parameters

      D%ix=D%ix_unbinned/D%binning_factor
      D%iy=D%iy_unbinned/D%binning_factor
      D%pixel = D%pixel * D%binning_factor

      X%xcen=X%xcen/D%binning_factor
      X%ycen=X%ycen/D%binning_factor

      X%Kxmin=X%Kxmin/D%binning_factor
      X%Kxmax=X%Kxmax/D%binning_factor
      X%Kymin=X%Kymin/D%binning_factor
      X%Kymax=X%Kymax/D%binning_factor

      X%Bxmin=X%Bxmin/D%binning_factor
      X%Bxmax=X%Bxmax/D%binning_factor
      X%Bymin=X%Bymin/D%binning_factor
      X%Bymax=X%Bymax/D%binning_factor

      return
      end

      subroutine init_datacol(X)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL) X
c
      X%Kxmin=0
      X%Kxmax=0
      X%Kymin=0
      X%Kymax=0
      X%Bxmin=0
      X%Bxmax=0
      X%Bymin=0
      X%Bymax=0
      X%pLim1=0
      X%pLim2=0
c
      X%idealback0=0.0
      X%idealback=0.0
      X%RList=0.0
      X%hklKoor=0.0
      X%vbin=0.0
      X%Ilimit=0.0
      X%vbins=0.0
      X%vbina=0.0
      X%Wil=0.0
c
      return
      end




import os
import numpy as np
import pyFAI.azimuthalIntegrator
from pyFAI import units
from pyFAI.opencl.peak_finder import OCL_PeakFinder


#from silx.opencl.codec.byte_offset import ByteOffset

class PF8(object):

    def __init__(self, det, ctx, verbose=False):

        #os.environ["PYOPENCL_COMPILER_OUTPUT"] = "1"
        #os.environ["PYOPENCL_CTX"] = '0:1'
        #import pyopencl
        self.ctx = ctx #pyopencl.create_some_context(interactive=False)
        self.det = pyFAI.detector_factory(det)



        if verbose:
            print(f"Using {self.ctx.devices[0]}")

    def setup_geom(self, mxcube_dic):

        # dimg = fimg.data.copy()
        # dimg[mask] = 0
        #print(mxcube_dic)
        ai = pyFAI.azimuthalIntegrator.AzimuthalIntegrator(dist=mxcube_dic['detector_distance'] / 1000.,
                                                           poni1=mxcube_dic['orgx'] / 1000.,  # (in m)
                                                           poni2=mxcube_dic['orgy'] / 1000.,
                                                           detector=self.det)
        kwargs = {"data": np.zeros(self.det.shape),
                  "npt": 1000,
                  "method": ("no", "csr", "cython"),
                  "polarization_factor": 0.99,
                  "unit": "r_mm", }
        ai.integrate1d(**kwargs)
        unit = units.to_unit("r_mm")
        image_size = self.det.shape[0] * self.det.shape[1]
        integrator = ai.setup_CSR(ai.detector.shape, 1000, mask=self.det.mask, unit=unit, split="no", scale=False)
        polarization = ai._cached_array["last_polarization"]

        self.pf = OCL_PeakFinder(integrator.lut,
                                 image_size=image_size,
                                 bin_centers=integrator.bin_centers,
                                 radius=ai._cached_array[unit.name.split("_")[0] + "_center"],
                                 mask=self.det.mask,
                                 ctx=self.ctx,
                                 block_size=512,
                                 unit=unit)

        self.kwargs = {"error_model": "azimuthal",
                       "polarization": polarization.array,
                       "polarization_checksum": polarization.checksum}

    def find_spots(self, data, params):
        res8 = self.pf.peakfinder8(**self.kwargs,
                                   data=data,
                                   cycle=params["cycles"],
                                   cutoff_pick=params["cutoff_pick"],
                                   noise=params["noise"],
                                   connected=params["connected"],
                                   patch_size=params["patch_size"])
        return res8#list(res8["pos1"].astype(np.float64)), list(res8["pos0"].astype(np.float64)), list(res8["intensity"].astype(np.float64))
